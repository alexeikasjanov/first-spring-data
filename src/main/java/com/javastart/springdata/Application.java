package com.javastart.springdata;
import com.javastart.springdata.entity.Account;
import com.javastart.springdata.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Transient;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.document.AbstractXlsView;
import sun.tools.jar.CommandLine;

import java.util.Map;

@SpringBootApplication
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AccountRepository accountRepository;

    @Transactional
    @Override
    public void run(String... args) throws Exception {
        jdbcTemplate.execute("INSERT INTO Account (id,name,email,bill)"+
                "VALUES (1,'Lori','lori@cat.xyz',2000)"); // sql request
        // after check reading from db
        jdbcTemplate.queryForMap("SELECT * FROM Account");
        Account accountById = findAccountById(1L);
        System.out.println(accountById);
//        for (int i =0 ; i < 10 ; i++){
//            accountRepository.save(new Account(null,"Lori"+i,
//                    "lori@cat.xyz",2000*i));
//        }
//
//        System.out.println(accountRepository.findAccountByName("Lori5"));
//        accountRepository.setNameFor(6L, "Bax");
//
//        System.out.println(accountRepository.findAccountBy("Lori5",10000));
    }

    private Account findAccountById(Long accountId){
        String query = "SELECT * FROM Account WHERE id=%s"; // %s - place for id
        Map<String,Object> resultSet = jdbcTemplate.queryForMap(String.format(query,accountId));
        // after from resultSet we get all values from columns
        Account account = new Account();
        account.setId(accountId);
        account.setName((String) resultSet.get("name"));
        account.setEmail((String) resultSet.get("email"));
        account.setBill((Integer) resultSet.get("bill"));
        return account;
    }
}
